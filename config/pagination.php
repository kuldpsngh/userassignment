<?php
/**
 * Record per page
 * 
 * @return paginate response
 */
return [
   "paginate" => 10,
];