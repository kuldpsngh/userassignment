function validation(){
    var userName = document.getElementById('username').value;
    var userEmail = document.getElementById('useremail').value;
    var userPassword = document.getElementById('userpassword').value;
    var userConPassword = document.getElementById('password-confirm').value;
    // var userStore = document.form.store;
    var emailValue = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(userName=="")
    {
        document.getElementById('errorname').innerHTML = "Please Fill the Name Field";
        document.getElementById('username').focus();
        return false;
    }
    if(userEmail==""){
        document.getElementById('erroremail').innerHTML = "Please Fill the Email Field";
        document.getElementById('useremail').focus();
        document.getElementById('errorname').innerHTML = "";
        return false;
    }
    if(!userEmail.match(emailValue)){
        document.getElementById('erroremail').innerHTML = "Please fill the valid Email";
        document.getElementById('useremail').focus();
        return false;
    }
    if(userPassword==""){
        document.getElementById('errorpass').innerHTML = "Please Fill the Password Field";
        document.getElementById('userpassword').focus();
        document.getElementById('erroremail').innerHTML = "";
        return false;
    }   
    if(userConPassword!=userPassword){
        document.getElementById('errorpasscon').innerHTML = "Please Fill the Confirm Password Field";
        document.getElementById('password-confirm').focus();
        document.getElementById('errormobile').innerHTML = "";
        return false;
    }
    var checked = $("input[type=radio]:checked").length;
    if(checked<1){
        document.getElementById('errorstore').innerHTML = "Please select atleast one checkbox";
        document.getElementById('errorpasscon').innerHTML = "";
        return false;
    }
}