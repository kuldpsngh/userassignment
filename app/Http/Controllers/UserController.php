<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Store;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UserValidation;

class UserController extends Controller
{
    /**
     * Display users list
     *
     * @return view 
     */
    public function index()
    {
        $users = User::with('store:id,name')->paginate(10);
        
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a user.
     *
     * @return view
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Show the form for editing the specified user details.
     *
     * @param  int  $id
     * @return view with variables 
     */
    public function edit($id)
    {
        $stores = Store::all();
        $user = User::findOrFail($id);
        
        return view('users.edit', compact('stores', 'user'));
    }

    /**
     * Update the specified user details.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Route to index page
     */
    public function update(UserValidation $request, $id)
    {
        $user = User::findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->store_id = $request->store;
        $user->update();

        session()->flash('message', 'User updated successfully!!');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified user details.
     *
     * @param  int  $id
     * @return Route to index page
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        session()->flash('message', 'User deleted successfully!!');
        return redirect()->route('users.index');
    }
}
