<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;

    /**
     * Store table name.
     *
     * @var string
     */
    protected $table = 'stores';

    /**
     * Fillable table columns.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * belongs-to Store with User
     * 
     * @return Object
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }


}
