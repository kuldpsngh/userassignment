<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}" type="text/css">
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <script src="{{asset('/js/bootstrap.min.js')}}"></script>
    <title>{!!__("User")!!}</title>
</head>
<body>
    <div class="container mt-5">
        <h1 class="text-center">{!! __('User List') !!}</h1>
        @if(session()->get('message'))
            <div class="alert alert-success">
            {{ session()->get('message') }}  
            </div>
        @endif
        @if(session()->get('error'))
            <div class="alert alert-danger">
            {{ session()->get('error') }}  
            </div>
        @endif
        <a href="{{url('/')}}" class="btn btn-md btn-info float-md-right" id="home-btn">{!! __('Home') !!}</a>
        <a href="{{url('/register')}}" class="btn btn-md btn-info float-md-right" id="add-btn">{!! __('Add User') !!}</a>
        <br/>
        <table class="table table-bordered mb-5">
            <thead>
                <tr class="table-success">
                    <th scope="col">#</th>
                    <th scope="col">{!! __('Name') !!}</th>
                    <th scope="col">{!! __('Email') !!}</th>
                    <th scope="col">{!! __('Store') !!}</th>
                    <th scope="col">{!! __('Action') !!}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @forelse($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td> {{$user->name}} </td>
                            <td> {{$user->email}} </td>
                            <td> {{$user['store']['name']}} </td>
                            <td>
                                <form action="{{ url('users/'. $user->id) }}" method="POST">
                                    <a href="{{url('users/'.$user->id.'/edit')}}" 
                                    class="btn btn-xs btn-primary"> {!! __('Edit') !!} </a>
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-xs btn-danger"> {!! __('Delete') !!} </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="5">{!! __('Data Not Available') !!}</td>
                        </tr>
                    @endforelse
                </tr>
            </tbody>
        </table>
        <div class="d-flex justify-content-center">
            {!! $users->links("pagination::bootstrap-4") !!}
        </div>
    </div>
</body>
</html>